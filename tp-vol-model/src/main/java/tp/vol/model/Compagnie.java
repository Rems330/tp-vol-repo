package tp.vol.model;

import java.util.ArrayList;

public class Compagnie {
	
	private String nomCompagnie;
	private ArrayList<Vol> listeVol = new ArrayList<Vol>();
	
	public String getNomCompagnie() {
		return nomCompagnie;
	}
	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}
	public ArrayList<Vol> getListeVol() {
		return listeVol;
	}
	public void setListeVol(ArrayList<Vol> listeVol) {
		this.listeVol = listeVol;
	}
	
	

}
