package tp.vol.model;

import java.util.Date;

public class Passager extends Personne{
	
	private String nom;
	private String prenoms;
	private Date dtNaissance;
	private String numIdentite;
	private String nationalite;
	private String civilite;
	private String typePI;
	private Date dateValidite;
	
	
	
	public Passager() {
		super();
	}


	public Passager(String nom, String prenoms) {
		super();
		this.nom = nom;
		this.prenoms = prenoms;
		System.out.println("Le nom de ce passager est " + nom + " et ses pr�nom sont " + prenoms);
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenoms() {
		return prenoms;
	}


	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}


	public String getNumIdentite() {
		return numIdentite;
	}


	public void setNumIdentite(String numIdentite) {
		this.numIdentite = numIdentite;
	}


	public String getNationalite() {
		return nationalite;
	}


	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}


	public String getCivilite() {
		return civilite;
	}


	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}


	public String getTypePI() {
		return typePI;
	}


	public void setTypePI(String typePI) {
		this.typePI = typePI;
	}


	public Date getDtNaissance() {
		return dtNaissance;
	}


	public void setDtNaissance(Date dtNaissance) {
		this.dtNaissance = dtNaissance;
	}


	public Date getDateValidite() {
		return dateValidite;
	}


	public void setDateValidite(Date dateValidite) {
		this.dateValidite = dateValidite;
	}






		
}
