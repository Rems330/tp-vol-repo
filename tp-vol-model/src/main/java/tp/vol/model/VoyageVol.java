package tp.vol.model;

public class VoyageVol {
	
	private int ordre;
	private Vol vol;
	private Voyage voyage;
	
	
	public VoyageVol() {
		super();
	}
	public VoyageVol(int ordre, Vol vol, Voyage voyage) {
		super();
		this.ordre = ordre;
		this.vol = vol;
		this.voyage = voyage;
	}
	public int getOrdre() {
		return ordre;
	}
	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}
	public Vol getVol() {
		return vol;
	}
	public void setVol(Vol vol) {
		this.vol = vol;
	}
	public Voyage getVoyage() {
		return voyage;
	}
	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}

}
