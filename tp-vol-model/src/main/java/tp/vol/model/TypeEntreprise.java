package tp.vol.model;

public enum TypeEntreprise {
	
		IND("Entreprise individuelle"), EIRL("Entreprise individuelle à responsabilité limitée"),
		SARL("Société à responsabilité limitée"), SAS("Société par actions simplifiée"),
		SASU("Société par actions simplifiée unipersonnelle"), SA("Société anonyme"), SNC("Société en nom collectif");

		private final String labelentreprise;

		private TypeEntreprise(String labelentreprise) {
			this.labelentreprise = labelentreprise;
		}

		public String getLabelpaiement() {
			return labelentreprise;
		}

}

