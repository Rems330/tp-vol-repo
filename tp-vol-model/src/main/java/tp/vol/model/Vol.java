package tp.vol.model;

import java.util.Date;

public class Vol {
	
	private Date dateDepartVol;
	private long numeroDeVol;
	private Date dateArrivee;
	private Boolean ouvert;
	private int nbPlaces;
	private Aeroport aeroportDepart;
	private Aeroport aeroportArrivee;
	private Compagnie compagnie;
	
	

	public Vol() {
		super();
	}

	public Vol(Date dateDepartVol, long numeroDeVol, Date dateArrivee, Boolean ouvert, int nbPlaces,
			Aeroport aeroportDepart, Aeroport aeroportArrivee, Compagnie compagnie) {
		super();
		this.dateDepartVol = dateDepartVol;
		this.numeroDeVol = numeroDeVol;
		this.dateArrivee = dateArrivee;
		this.ouvert = ouvert;
		this.nbPlaces = nbPlaces;
		this.aeroportDepart = aeroportDepart;
		this.aeroportArrivee = aeroportArrivee;
		this.compagnie = compagnie;
	}

	public Aeroport getAeroportDepart() {
		return aeroportDepart;
	}

	public void setAeroportDepart(Aeroport aeroportDepart) {
		this.aeroportDepart = aeroportDepart;
	}

	public Aeroport getAeroportArrivee() {
		return aeroportArrivee;
	}

	public void setAeroportArrivee(Aeroport aeroportArrivee) {
		this.aeroportArrivee = aeroportArrivee;
	}
	
	public Date getDateDepartVol() {
		return dateDepartVol;
	}
	public void setDateDepartVol(Date dateDepartVol) {
		this.dateDepartVol = dateDepartVol;
	}
	public long getNumeroDeVol() {
		return numeroDeVol;
	}
	public void setNumeroDeVol(long numeroDeVol) {
		this.numeroDeVol = numeroDeVol;
	}
	public Date getDateArrivee() {
		return dateArrivee;
	}
	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}
	public Boolean getOuvert() {
		return ouvert;
	}
	public void setOuvert(Boolean ouvert) {
		this.ouvert = ouvert;
	}
	public int getNbPlaces() {
		return nbPlaces;
	}
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public Compagnie getCompagnie() {
		return compagnie;
	}

	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}
	
}
