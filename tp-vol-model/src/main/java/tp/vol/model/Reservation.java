package tp.vol.model;

import java.util.Date;

public class Reservation {
	
	private String numeroDeReservation;
	private Boolean statutDeReservation;
	private float tarifReservation;
	private float tauxTVA;
	private Date dateReservation;
	private Client client;
	private Passager passager;
	private Voyage voyage;
	
	
	
	public Reservation() {
		super();
	}


	public Reservation(String numeroDeReservation, Boolean statutDeReservation, float tarifReservation, float tauxTVA,
			Date dateReservation, Client client, Passager passager, Voyage voyage) {
		super();
		this.numeroDeReservation = numeroDeReservation;
		this.statutDeReservation = statutDeReservation;
		this.tarifReservation = tarifReservation;
		this.tauxTVA = tauxTVA;
		this.dateReservation = dateReservation;
		this.client = client;
		this.passager = passager;
		this.voyage = voyage;
	}
	
	
	public String getNumeroDeReservation() {
		return numeroDeReservation;
	}
	public void setNumeroDeReservation(String numeroDeReservation) {
		this.numeroDeReservation = numeroDeReservation;
	}
	public Boolean getStatutDeReservation() {
		return statutDeReservation;
	}
	public void setStatutDeReservation(Boolean statutDeReservation) {
		this.statutDeReservation = statutDeReservation;
	}
	public float getTarifReservation() {
		return tarifReservation;
	}
	public void setTarifReservation(float tarifReservation) {
		this.tarifReservation = tarifReservation;
	}
	public float getTauxTVA() {
		return tauxTVA;
	}
	public void setTauxTVA(float tauxTVA) {
		this.tauxTVA = tauxTVA;
	}
	public Date getDateReservation() {
		return dateReservation;
	}
	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Passager getPassager() {
		return passager;
	}
	public void setPassager(Passager passager) {
		this.passager = passager;
	}
	public Voyage getVoyage() {
		return voyage;
	}
	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}
	

}
