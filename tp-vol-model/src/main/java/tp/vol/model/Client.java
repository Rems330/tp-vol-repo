package tp.vol.model;

public abstract class Client extends Personne {

	private MoyenPaiement moyenPaiement;
	private Adresse adresse;
	
	

	public Client() {
		super();
	}

	public MoyenPaiement getMoyenPaiement() {
		System.out.println("Son moyen de paiement est par " + moyenPaiement);
		return moyenPaiement;
	}

	public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
		
		this.moyenPaiement = moyenPaiement;
	}

	public Adresse getAdressefacturation() {
		return adresse;
	}

	public void setAdressefacturation(Adresse adresse) {
		this.adresse = adresse;
	}

	
}
