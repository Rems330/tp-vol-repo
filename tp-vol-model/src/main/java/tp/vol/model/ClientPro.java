package tp.vol.model;

public class ClientPro extends Client{
	
	private String nomEntreprise;
	private TypeEntreprise typeEntreprise;
	private Long numeroSiret;
	private String numTVA;
	
	
	
	public ClientPro() {
		super();
	}


	public ClientPro(String nomEntreprise, TypeEntreprise typeEntreprise, Long numeroSiret, String numTVA) {
		super();
		this.nomEntreprise = nomEntreprise;
		this.typeEntreprise = typeEntreprise;
		this.numeroSiret = numeroSiret;
		this.numTVA = numTVA;
		System.out.println("L'entreprise" + nomEntreprise + " de type " + typeEntreprise + " dont le numero Siret est " + numeroSiret + " avec un numero de TVA de " + numTVA);
	}
	
	
	public Long getNumeroSiret() {
		return numeroSiret;
	}
	public void setNumeroSiret(Long numeroSiret) {
		this.numeroSiret = numeroSiret;
	}
	public String getNomEntreprise() {
		return nomEntreprise;
	}
	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	public String getNumTVA() {
		return numTVA;
	}
	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}
	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}
	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}
	


}
