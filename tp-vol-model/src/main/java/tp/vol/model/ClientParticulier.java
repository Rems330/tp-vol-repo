package tp.vol.model;

import tp.vol.model.Civilite;

public class ClientParticulier extends Client {

	private Civilite civilite;
	private String nom;
	private String prenoms;


	public ClientParticulier() {
		super();
	}

	public ClientParticulier(Civilite civilite, String nom, String prenoms) {
		super();
		this.civilite = civilite;
		this.nom = nom;
		this.prenoms = prenoms;
		System.out.println("Sa civilite est " + civilite);
		System.out.println("Son nom est " + nom);
		System.out.println("Ses prenoms sont " + prenoms);
	}

	public Civilite getCivilite() {
		return civilite;
	}
	
	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenoms() {
		return prenoms;
	}

	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}

}
