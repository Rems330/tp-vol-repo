package tp.vol.model;

public class Ville {
	
	private String nomVille;
	
	public Ville() {
		super();

	}

	
	public Ville(String nomVille) {
		super();
		this.nomVille = nomVille;
		System.out.println("Ville : " + nomVille);
	}

	public String getNomVille() {
		return nomVille;
	}

	public void setNomVille(String nomVille) {
		this.nomVille = nomVille;
	}
	

}
