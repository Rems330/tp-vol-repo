package tp.vol.model;

public enum MoyenPaiement {
	CB("Carte Bleue"),CHQ("Chèque"),ESP("Espèce"),PP("Paypal");
	
	private final String labelpaiement;
	
	private MoyenPaiement(String labelpaiement) {
		this.labelpaiement = labelpaiement;
	}
	
	public String getLabelpaiement() {
		return labelpaiement;
	}

}