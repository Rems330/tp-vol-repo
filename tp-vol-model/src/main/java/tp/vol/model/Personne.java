package tp.vol.model;

public abstract class Personne {
	
	private String mail;
	private String telephone;
	private Adresse adresseperso;
	
	public String getMail() {
		System.out.println("Son adresse mail est " + mail);
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelephone() {
		System.out.println("Son adresse telephone est " + telephone);
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Adresse getAdresseperso() {
		return adresseperso;
	}
	public void setAdresseperso(Adresse adresseperso) {
		this.adresseperso = adresseperso;
	}

}
