package tp.vol.model;

import java.util.ArrayList;

public class Aeroport {
	
	private String codeAeroport;
	private ArrayList<Ville> listevilles = new ArrayList<Ville>();
	private ArrayList<Vol> listevol = new ArrayList<Vol>();
		

	public Aeroport(String codeAeroport) {
		super();
		this.codeAeroport = codeAeroport;
	}

	public ArrayList<Vol> getListevol() {
		return listevol;
	}

	public void setListevol(ArrayList<Vol> listevol) {
		this.listevol = listevol;
	}

	public ArrayList<Ville> getListevilles() {
		return listevilles;
	}

	public void setListevilles(ArrayList<Ville> listevilles) {
		this.listevilles = listevilles;
	}

	public String getCodeAeroport() {
		return codeAeroport;
	}

	public void setCodeAeroport(String codeAeroport) {
		this.codeAeroport = codeAeroport;
	}
	

}
