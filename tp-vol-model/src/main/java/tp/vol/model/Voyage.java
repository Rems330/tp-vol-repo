package tp.vol.model;

import java.util.ArrayList;

public class Voyage {
	private String idVoyage;
	private ArrayList<VoyageVol> listevoyagevol = new ArrayList<VoyageVol>();
	private ArrayList<Reservation> listereservation = new ArrayList<Reservation>();
	
	
	public ArrayList<VoyageVol> getListevoyagevol() {
		return listevoyagevol;
	}
	
	public void setListevoyagevol(ArrayList<VoyageVol> listevoyagevol) {
		this.listevoyagevol = listevoyagevol;
	}

	public ArrayList<Reservation> getListereservation() {
		return listereservation;
	}

	public void setListereservation(ArrayList<Reservation> listereservation) {
		this.listereservation = listereservation;
	}

	public String getIdVoyage() {
		return idVoyage;
	}

	public void setIdVoyage(String idVoyage) {
		this.idVoyage = idVoyage;
	}

}
