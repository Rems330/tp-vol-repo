package tp.vol.test;

import tp.vol.Application;
import tp.vol.model.Aeroport;

public class TestAeroport {

	public static void main(String[] args) {
		Aeroport madrid = new Aeroport("mad");
		
		Application.getInstance().getAeroportDao().create(madrid);
		
		madrid = Application.getInstance().getAeroportDao().findById("mad");
		
		System.out.println(madrid.getCodeAeroport());
		
		madrid = Application.getInstance().getAeroportDao().findById("mad");
		System.out.println(madrid.getCodeAeroport());
		
		Application.getInstance().getAeroportDao().delete(madrid);
		
		

	}

}
