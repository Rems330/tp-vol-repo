package tp.vol.test;

import java.util.List;

import sopra.formation.model.Eleve;
import tp.vol.model.Civilite;
import tp.vol.model.Passager;
import tp.vol.Application;
import tp.vol.dao.IPassagerDao;

public class PassagerDaoSqlTest {
	public static void main(String[] args) {
		
		IPassagerDao passagerDao = Application.getInstance().getPassagerDao();
		
		
		List<Passager> passagers = passagerDao.findAll();
		
		for (Passager passager : passagers) {
			System.out.println(passager.getNom());
		}
		
		///////////////////////////////////////////////////////////////////////////////////////////
		Passager passager1 = passagerDao.findById("mehdi.legamer33@gmail.com");

		System.out.println(passager1.getPrenoms());
		
		////////////////////////////////////////////////////////////////////////////////////////////
		
		Passager remy = new Passager();
		remy.setPrenoms("Remy");
		remy.setNom("Nala");
		remy.setCivilite(Civilite.M);
		remy.setMail("remydu33@gmail.com");
		remy.setTelephone("0645978238");
		remy.setNationalite("française");
		remy.setDtNaissance(15/04/1992);
		remy.setTypePI("Passeport");
		remy.setNumIdentite("ME44828738");
		remy.setDateValidite(22/07/22);
		
		passagerDao.create(remy);
		
		for (Passager passager : passagers) {
			System.out.println(passager.getNom());

		}
		
		////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
	}
}
