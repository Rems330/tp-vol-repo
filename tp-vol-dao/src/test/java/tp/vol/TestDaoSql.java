package tp.vol;

import tp.vol.model.Civilite;
import tp.vol.model.ClientParticulier;
import tp.vol.model.MoyenPaiement;

public class TestDaoSql {

	public static void main(String[] args) {
		ClientParticulier eric = new ClientParticulier();
		eric.setAdresseperso(Application.getInstance().getAdresseDao().findById(1L));
		eric.setCivilite(Civilite.M);
		eric.setMail("eric87@gmail.com");
		eric.setMoyenPaiement(MoyenPaiement.CB);
		eric.setNom("Sultan");
		eric.setPrenoms("Eric");
		eric.setTelephone("0666889977");
		
		Application.getInstance().getClientParticulierDao().create(eric);
		
		
//		Application.getInstance().getFormateurDao().create(eric);
	//	
//		eric = Application.getInstance().getFormateurDao().findById(1L);
	//	
//		System.out.println(eric.getNom()+" " +eric.getPrenom());
	//	
//		eric.setNom("BRUEL");
//		eric.setPrenom("Erica");
	//	
//		Application.getInstance().getFormateurDao().update(eric);
	//	
//		eric = Application.getInstance().getFormateurDao().findById(1L);
	//	
//		System.out.println(eric.getNom()+" " +eric.getPrenom());
	//	
	//
	//	
//		eric = Application.getInstance().getFormateurDao().findById(1L);
	//	
//		System.out.println(eric);

	}

}
