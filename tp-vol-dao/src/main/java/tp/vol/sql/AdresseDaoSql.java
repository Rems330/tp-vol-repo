package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.IAdresseDao;
import tp.vol.model.Adresse;


public class AdresseDaoSql implements IAdresseDao{

	@Override
	public List<Adresse> findAll() {
		List<Adresse> adresses = new ArrayList<Adresse>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT ID_ADRESSE, VOIE, COMPLEMENT, CODE_POSTAL, VILLE, PAYS FROM adresses");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long idAdresse = rs.getLong("ID_ADRESSE");
				String voie = rs.getString("VOIE");
				String complement = rs.getString("COMPLEMENT");
				String codePostal = rs.getString("CODE_POSTAL");
				String ville = rs.getString("VILLE");
				String pays = rs.getString("PAYS");

				
						

				Adresse adresse = new Adresse();
				adresse.setIdAdresse(idAdresse);
				adresse.setVoie(voie);
				adresse.setComplement(complement);
				adresse.setCodePostal(codePostal);
				adresse.setVille(ville);
				adresse.setPays(pays);
				

				adresses.add(adresse);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return adresses;
	}

	@Override
	public Adresse findById(Long id) {
		Adresse adresse = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT ID_ADRESSE, VOIE, COMPLEMENT, CODE_POSTAL, VILLE, PAYS FROM adresses WHERE ID_ADRESSE = ?");

			ps.setLong(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Long idAdresse = rs.getLong("ID_ADRESSE");
				String voie = rs.getString("VOIE");
				String complement = rs.getString("COMPLEMENT");
				String codePostal = rs.getString("CODE_POSTAL");
				String ville = rs.getString("VILLE");
				String pays = rs.getString("PAYS");

				
						

				adresse = new Adresse();
				adresse.setIdAdresse(idAdresse);
				adresse.setVoie(voie);
				adresse.setComplement(complement);
				adresse.setCodePostal(codePostal);
				adresse.setVille(ville);
				adresse.setPays(pays);
				
				
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return adresse;
	}

	@Override
	public void create(Adresse obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO adresses(ID_ADRESSE, VOIE, COMPLEMENT, CODE_POSTAL, VILLE, PAYS) VALUES (?,?,?,?,?,?)");
			
			ps.setLong(1, obj.getIdAdresse());
			ps.setString(2, obj.getVoie() );
			ps.setString(3, obj.getComplement());
			ps.setString(4, obj.getCodePostal());
			ps.setString(5, obj.getVille());
			ps.setString(6, obj.getPays());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(Adresse obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE adresses SET voie = ?, complement = ?, code_postal = ?, ville = ?, pays = ? WHERE id_adresse = ?");
			
			
			
			ps.setString(1, obj.getVoie() );
			ps.setString(2, obj.getComplement());
			ps.setString(3, obj.getCodePostal());
			ps.setString(4, obj.getVille());
			ps.setString(5, obj.getPays());
			ps.setLong(6,obj.getIdAdresse());

			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Adresse obj) {
		deleteById(obj.getIdAdresse());
		
	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE adresses WHERE id_adresse = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
