package tp.vol.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.IPassagerDao;
import tp.vol.model.Passager;

public class PassagerDaoSql implements IPassagerDao {

	@Override
	public List<Passager> findAll() {

		List<Passager> passagers = new ArrayList<Passager>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT mail, telephone, civilite, nom, prenoms, dt_naissance, num_identite, nationalite, type_pi,date_validite_pi FROM passagers");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				
				Long id = rs.getLong("id");

				String mail = rs.getString("mail");
				Long telephone = rs.getLong("telephone");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				Date dtnaissance = rs.getDate("dt_naissance");
				String numidentite = rs.getString("num_identite");
				String nationalite = rs.getString("nationalite");
				String typepi = rs.getString("typepi");
				Date datevaliditepi = rs.getDate("date_validite_pi");

				Passager passager1 = new Passager();
				passager1.setMail(mail);
				passager1.setTelephone(telephone.toString());
				passager1.setCivilite(civilite);
				passager1.setNom(nom);
				passager1.setPrenoms(prenoms);
				passager1.setDtNaissance(dtnaissance);
				passager1.setNumIdentite(numidentite);
				passager1.setNationalite(nationalite);
				passager1.setTypePI(typepi);
				passager1.setDateValidite(datevaliditepi);

				passagers.add(passager1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passagers;
	}

	@Override
	public Passager findById(String mail) {
		Passager passager = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT mail, telephone, civilite, nom, prenoms, dt_naissance, num_identite, nationalite, type_pi,date_validite_pi FROM passagers WHERE mail = ?");

			ps.setString(1, mail);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				String telephone = rs.getString("telephone");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				Date dtnaissance = rs.getDate("date");
				String numidentite = rs.getString("num_idendite");
				String nationalite = rs.getString("nationalite");
				String typepi = rs.getString("type_pi");
				Date datevaliditepi = rs.getDate("date_validite_pi");

				Passager passager1 = new Passager();
				passager1.setTelephone(telephone.toString());
				passager1.setCivilite(civilite);
				passager1.setNom(nom);
				passager1.setPrenoms(prenoms);
				passager1.setDtNaissance(dtnaissance);
				passager1.setNumIdentite(numidentite);
				passager1.setNationalite(nationalite);
				passager1.setTypePI(typepi);
				passager1.setDateValidite(datevaliditepi);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passager;
	}

	@Override
	public void create(Passager obj) {

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO passagers (mail, telephone, civilite, nom, prenoms, dt_naissance, num_identite, nationalite, type_pi,date_validite_pi) VALUES (?,?,?,?,?,?,?,?,?,?)");

			ps.setString(1, obj.getMail());
			ps.setString(2, obj.getTelephone());
			ps.setString(3, obj.getCivilite().toString());
			ps.setString(4, obj.getNom());
			ps.setString(5, obj.getPrenoms());
			ps.setDate(6, new Date(obj.getDtNaissance().getTime()));
			ps.setString(7, obj.getNumIdentite());
			ps.setString(8, obj.getNationalite());
			ps.setString(9, obj.getTypePI());
			ps.setDate(10, new Date(obj.getDateValidite().getTime()));

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Passager obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"UPDATE passagers SET mail = ?, telephone = ?, civilitepassager = ?, nom = ?, prenoms = ?,dt_naissance = ?, num_identite = ?, nationalite = ?, type_pi = ?,date_validite_pi = ? WHERE mail = ?");

			ps.setString(1, obj.getMail());
			ps.setString(2, obj.getTelephone());
			ps.setString(3, obj.getCivilite().toString());
			ps.setString(4, obj.getNom());
			ps.setString(5, obj.getPrenoms());
			ps.setDate(6, new Date(obj.getDtNaissance().getTime()));
			ps.setString(7, obj.getNumIdentite());
			ps.setString(8, obj.getNationalite());
			ps.setString(9, obj.getTypePI());
			ps.setDate(10, new Date(obj.getDateValidite().getTime()));

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Passager obj) {
		deleteById(obj.getMail());
	}

	@Override
	public void deleteById(String mail) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE passager WHERE mail = ?");

			ps.setString(1, mail);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
