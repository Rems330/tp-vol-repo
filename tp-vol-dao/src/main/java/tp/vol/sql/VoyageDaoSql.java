package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.IVoyageDao;
import tp.vol.model.Voyage;


public class VoyageDaoSql implements IVoyageDao{

	@Override
	public List<Voyage> findAll() {
		List<Voyage> voyages = new ArrayList<Voyage>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT ID_VOYAGE FROM voyages");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String idVoyage = rs.getString("ID_VOYAGE");

				Voyage voyage = new Voyage();
				voyage.setIdVoyage(idVoyage);
				
				
	
				voyages.add(voyage);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return voyages;
	}

	@Override
	public Voyage findById(String id) {
		Voyage voyage = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT ID_VOYAGE FROM voyages WHERE id_voyage = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				String idVoyage = rs.getString("ID_VOYAGE");

				voyage = new Voyage();
				voyage.setIdVoyage(idVoyage);

				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return voyage;
	}

	@Override
	public void create(Voyage obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO voyages (ID_VOYAGE) VALUES (?)");
			
			ps.setString(1, obj.getIdVoyage());
	
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(Voyage obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE voyages SET id_voyage = ? WHERE id_voyage = ?");
			
			
			
			ps.setString(1, obj.getIdVoyage() );


			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Voyage obj) {
		deleteById(obj.getIdVoyage());
		
	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE voyages WHERE ID_VOYAGE = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
