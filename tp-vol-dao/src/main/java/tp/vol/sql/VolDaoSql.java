package tp.vol.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.IVolDao;
import tp.vol.model.Vol;

public class VolDaoSql implements IVolDao {

	@Override
	public List<Vol> findAll() {
		List<Vol> vol = new ArrayList<Vol>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT numero FROM vols");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				Long numero = rs.getLong("numero");
		//		Compagnie compagnie = Compagnie.valueOf(rs.getString("Compagnie"));
				String aeroportCodeDepart = rs.getString("aeroportCodeDepart");
				String aeroportCodeArrivee = rs.getString("aeroportCodeArrivee");
				Date dateDepartVol = rs.getDate("dateDepartVol");
				Date dateArriveeVol = rs.getDate("dateArriveeVol");
				Boolean ouvert = rs.getBoolean("ouvert");
				Long nbPlaces = rs.getLong("nb_places");
				

		//		Vol vol = new Vol(numero);	

		//		vols.add(vol);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return vol;
	}

	@Override
	public Vol findById(Long numero) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Vol obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO vol (numero, compagnie_nom_compagnie, aeroport_code_depart,"
							+ " aeroport_code_arrivee, date_depart, date_arrivee, ouvert, nb_places) VALUES (?,?,?,?,?,?,?,?)");

			ps.setLong(1, obj.getNumeroDeVol());
			if(obj.getCompagnie() != null && obj.getCompagnie().getNomCompagnie() != null) {
				ps.setString(2, obj.getCompagnie().getNomCompagnie());
			} else {
				ps.setNull(2, Types.VARCHAR);
			}
			if(obj.getAeroportDepart() != null && obj.getAeroportDepart().getCodeAeroport() != null) {
				ps.setString(3, obj.getAeroportDepart().getCodeAeroport());
			} else {
				ps.setNull(3, Types.VARCHAR);
			}
			if(obj.getAeroportArrivee() != null && obj.getAeroportArrivee().getCodeAeroport() != null) {
				ps.setString(4, obj.getAeroportArrivee().getCodeAeroport());
			} else {
				ps.setNull(4, Types.VARCHAR);
			}
			ps.setDate(5, new Date(obj.getDateDepartVol().getTime()));
			ps.setDate(6, new Date(obj.getDateArrivee().getTime()));
			ps.setBoolean(7, new Boolean(obj.getOuvert()));
			ps.setInt(8, new Integer(obj.getNbPlaces()));
			
			if(obj.getAeroportDepart() != null && obj.getAeroportArrivee().getCodeAeroport() != null) {
				ps.setString(3, obj.getAeroportDepart().getCodeAeroport());
			} else {
				ps.setNull(6, Types.VARCHAR);
			}

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
	}

	@Override
	public void update(Vol obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("UPDATE vol SET compagnie_nom_compagnie = ?, aeroport_code_depart = ?, aeroport_code_arrivee = ?, "
							+ "date_depart = ?, date_arrivee = ?, ouvert = ?, nb_places = ? WHERE numero = ?");

			if(obj.getCompagnie() != null && obj.getCompagnie().getNomCompagnie() != null) {
				ps.setString(1, obj.getCompagnie().getNomCompagnie());
			} else {
				ps.setNull(1, Types.VARCHAR);
			}
			if(obj.getAeroportDepart() != null && obj.getAeroportDepart().getCodeAeroport() != null) {
				ps.setString(2, obj.getAeroportDepart().getCodeAeroport());
			} else {
				ps.setNull(2, Types.VARCHAR);
			}
			if(obj.getAeroportArrivee() != null && obj.getAeroportArrivee().getCodeAeroport() != null) {
				ps.setString(3, obj.getAeroportArrivee().getCodeAeroport());
			} else {
				ps.setNull(3, Types.VARCHAR);
			}
			ps.setDate(4, new Date(obj.getDateDepartVol().getTime()));
			ps.setDate(5, new Date(obj.getDateArrivee().getTime()));
			ps.setBoolean(6, obj.getOuvert());
			ps.setLong(7, obj.getNbPlaces());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Vol obj) {
		deleteById(obj.getNumeroDeVol());
		
	}

	@Override
	public void deleteById(Long numero) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE vol WHERE numero = ?");

			ps.setLong(1, numero);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
