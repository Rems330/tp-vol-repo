package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.IVilleDao;
import tp.vol.model.Ville;


public class VilleDaoSql implements IVilleDao{

	@Override
	public List<Ville> findAll() {
		List<Ville> villes = new ArrayList<Ville>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT NOM FROM villes");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String nom = rs.getString("NOM");

				Ville ville = new Ville();
				ville.setNomVille(nom);

				

				villes.add(ville);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return villes;
	}

	@Override
	public Ville findById(String id) {
		Ville ville = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT NOM FROM villes WHERE NOM = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				String nom = rs.getString("NOM");

				ville = new Ville();
				ville.setNomVille(nom);

				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return ville;
	}

	@Override
	public void create(Ville obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO villes (NOM) VALUES (?)");
			
			ps.setString(1, obj.getNomVille());
	
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(Ville obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE villes SET NOM = ? WHERE NOM = ?");
			
			
			
			ps.setString(1, obj.getNomVille() );


			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Ville obj) {
		deleteById(obj.getNomVille());
		
	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE villes WHERE NOM = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
}


