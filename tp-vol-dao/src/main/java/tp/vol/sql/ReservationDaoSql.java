package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.IReservationDao;
import tp.vol.model.Client;
import tp.vol.model.Passager;
import tp.vol.model.Reservation;

public class ReservationDaoSql implements IReservationDao {

	@Override
	public List<Reservation> findAll() {

		List<Reservation> reservations = new ArrayList<Reservation>();

		Connection connection = null;

		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT numero, statut, tarif, taux_tva, date_reservation, clients_mail, passagers_mail, voyages_id_voyage FROM reservations");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				String numerodereservation = rs.getString("numero");

				boolean statutdereservation = rs.getBoolean("statut");
				float tarifreservation = rs.getFloat("tarif");
				float tauxtva = rs.getFloat("taux_tva");
				Date datereservation = rs.getDate("date_reservation");
				String clientsmail = rs.getString("clients_mail");
				String passagersmail = rs.getString("passagers_mail");
				String voyagesidvoyage = rs.getString("voyages_id_voyage");

				Reservation reservation1 = new Reservation();
				reservation1.setNumeroDeReservation(numerodereservation);
				reservation1.setStatutDeReservation(statutdereservation);
				reservation1.setTarifReservation(tarifreservation);
				reservation1.setTauxTVA(tauxtva);
				reservation1.setDateReservation(datereservation);

				if (passagersmail != null) {
					Passager passager = Application.getInstance().getPassagerDao().findById(passagersmail);
					passager.setMail(passagersmail);

				}
				
				if (clientsmail != null) {
					Client client = Application.getInstance().getClientParticulierDao().findById(clientsmail);
					client.setMail(clientsmail);

				}
				
				if (clientsmail != null) {
					Client client = Application.getInstance().getClientProDao().findById(clientsmail);
					client.setMail(clientsmail);

				}

				reservations.add(reservation1);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return reservations;
		}
			
	}

	@Override
	public Reservation findById(String numero) {
		Reservation reservation = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT numero, statut, tarif, taux_tva, date_reservation, clients_mail, passagers_mail, voyages_id_voyage FROM reservations WHERE numero = ?");

			ps.setString(1, numero);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				boolean statutdereservation = rs.getBoolean("statut");
				float tarifreservation = rs.getFloat("tarif");
				float tauxtva = rs.getFloat("taux_tva");
				Date datereservation = rs.getDate("date_reservation");
				String clientsmail = rs.getString("clients_mail");
				String passagersmail = rs.getString("passagers_mail");
				String voyagesidvoyage = rs.getString("voyages_id_voyage");

				Reservation reservation1 = new Reservation();
				reservation1.setStatutDeReservation(statutdereservation);
				reservation1.setTarifReservation(tarifreservation);
				reservation1.setTauxTVA(tauxtva);
				reservation1.setDateReservation(datereservation);

				if (passagersmail != null) {
					Passager passager = Application.getInstance().getPassagerDao().findById(passagersmail);
					passager.setMail(passagersmail);

				}
				
				if (clientsmail != null) {
					Client client = Application.getInstance().getClientParticulierDao().findById(clientsmail);
					client.setMail(clientsmail);

				}
				
				if (clientsmail != null) {
					Client client = Application.getInstance().getClientProDao().findById(clientsmail);
					client.setMail(clientsmail);

				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return reservation;
	}

	@Override
	public void create(Reservation obj) {

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO reservations (numero, statut, tarif, taux_tva, date_reservation, clients_mail, passagers_mail, voyages_id_voyage) VALUES (?,?,?,?,?,?,?,?)");

			ps.setString(1, obj.getNumeroDeReservation());
			ps.setBoolean(2, obj.getStatutDeReservation());
			ps.setFloat(3, obj.getTarifReservation());
			ps.setFloat(4, obj.getTauxTVA());
			ps.setDate(5, new Date(obj.getDateReservation().getTime()));

			if (obj.getPassager() != null && obj.getPassager().getMail() != null) {
				ps.setString(6, obj.getNumeroDeReservation());
			} else {
				ps.setNull(6, Types.VARCHAR);
			}
			
			if (obj.getClient() != null && obj.getClient().getMail() != null) {
				ps.setString(7, obj.getNumeroDeReservation());
			} else {
				ps.setNull(7, Types.VARCHAR);
			}

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Reservation obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"UPDATE reservations SET numero = ?, statut = ?, tarif = ?, taux_tva = ?, date_reservation = ?, clients_mail = ?, passagers_mail = ?, voyages_id_voyage = ? WHERE numero = ?");
			
			ps.setString(1, obj.getNumeroDeReservation());
			ps.setBoolean(2, obj.getStatutDeReservation());
			ps.setFloat(3, obj.getTarifReservation());
			ps.setFloat(4, obj.getTauxTVA());
			ps.setDate(5, new Date(obj.getDateReservation().getTime()));
			
			if (obj.getPassager() != null && obj.getPassager().getMail() != null) {
				ps.setString(6, obj.getNumeroDeReservation());
			} else {
				ps.setNull(6, Types.VARCHAR);
			}
			
			if (obj.getClient() != null && obj.getClient().getMail() != null) {
				ps.setString(7, obj.getNumeroDeReservation());
			} else {
				ps.setNull(7, Types.VARCHAR);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		}
	}

	@Override
	public void delete(Reservation obj) {
		deleteById(obj.getNumeroDeReservation());
	}

	@Override
	public void deleteById(String numero) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE reservations WHERE numero = ?");

			ps.setString(1, numero);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
