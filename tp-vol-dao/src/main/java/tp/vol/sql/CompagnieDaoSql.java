package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.ICompagnieDao;
import tp.vol.model.Compagnie;

public class CompagnieDaoSql implements ICompagnieDao {

	@Override
	public List<Compagnie> findAll() {
		List<Compagnie> compagnies = new ArrayList<Compagnie>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT nom_compagnie FROM compagnies");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				String nomCompagnie = rs.getString("nom_compagnie");

				Compagnie maCompagnie = new Compagnie();
				maCompagnie.setNomCompagnie(nomCompagnie);

				compagnies.add(maCompagnie);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return compagnies;
	}

	@Override
	public Compagnie findById(String nomCompagnie) {
		
		Compagnie compagnie = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT nom_compagnie FROM compagnies WHERE nom_compagnie = ?");

			ps.setString(1, nomCompagnie);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				 nomCompagnie = rs.getString("nom_compagnie"); 
				 //TODO verifier code où deja créé
				compagnie = new Compagnie();
				compagnie.setNomCompagnie(nomCompagnie);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
		return compagnie;
	}

	@Override
	public void create(Compagnie obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO compagnies (code)VALUES (?)");
			
			ps.setString(1,obj.getNomCompagnie());
			
			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(Compagnie obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("UPDATE compagnies SET nom_compagnie = ? WHERE nom_compagnie = ?");

			ps.setString(1, obj.getNomCompagnie());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Compagnie obj) {
		deleteById(obj.getNomCompagnie());
		
	}

	@Override
	public void deleteById(String nomCompagnie) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE compagnies WHERE nom_compagnie = ?");

			ps.setString(1, nomCompagnie);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
	

}
