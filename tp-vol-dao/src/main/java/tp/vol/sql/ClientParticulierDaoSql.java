package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;


import tp.vol.Application;
import tp.vol.dao.IClientParticulierDao;
import tp.vol.model.Adresse;
import tp.vol.model.Civilite;
import tp.vol.model.ClientParticulier;
import tp.vol.model.MoyenPaiement;

public class ClientParticulierDaoSql implements IClientParticulierDao{

	@Override
	public List<ClientParticulier> findAll() {
		List<ClientParticulier> clientsParticuliers = new ArrayList<ClientParticulier>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT mail, telephone, moyen_paiement, civilite, nom, prenoms, type_instance, ADRESSE_ID_ADRESSE FROM clients");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String moyenPaiement = rs.getString("moyen_paiement");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				String typeInstance = rs.getString("type_instance");
				Long idAdresse = rs.getLong("ADRESSE_ID_ADRESSE");
				
						

				ClientParticulier clientParticulier = new ClientParticulier();
				clientParticulier.setMail(mail);
				clientParticulier.setTelephone(telephone);
				clientParticulier.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
				clientParticulier.setCivilite(Civilite.valueOf(civilite));
				clientParticulier.setNom(nom);
				clientParticulier.setPrenoms(prenoms);
				
				
				if (idAdresse != null) {
					Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresse);
					clientParticulier.setAdresseperso(adresse);
                }

				

				clientsParticuliers.add(clientParticulier);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return clientsParticuliers;
	}

	@Override
	public ClientParticulier findById(String id) {
		ClientParticulier clientParticulier = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT  mail, telephone, moyen_paiement, civilite, nom, prenoms, type_instance, ADRESSE_ID_ADRESSE FROM clients WHERE id = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String moyenPaiement = rs.getString("moyen_paiement");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				String typeInstance = "particulier";
				Long idAdresse = rs.getLong("ADRESSE_ID_ADRESSE");
				
				clientParticulier = new ClientParticulier();
				clientParticulier.setMail(mail);
				clientParticulier.setTelephone(telephone);
				clientParticulier.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
				clientParticulier.setCivilite(Civilite.valueOf(civilite));
				clientParticulier.setNom(nom);
				clientParticulier.setPrenoms(prenoms);
				
				if (idAdresse != null) {
					Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresse);
					clientParticulier.setAdresseperso(adresse);
                }
				
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return clientParticulier;
	}

	@Override
	public void create(ClientParticulier obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO clients(mail, telephone, moyen_paiement, civilite, nom, prenoms, type_instance, ADRESSE_ID_ADRESSE) VALUES (?,?,?,?,?,?,?,?)");
			
			ps.setString(1, obj.getMail());
			ps.setString(2, obj.getTelephone() );
			ps.setString(3, obj.getMoyenPaiement().toString());
			ps.setString(4, obj.getCivilite().toString());
			ps.setString(5, obj.getNom());
			ps.setString(6, obj.getPrenoms());
			ps.setString(7, "particulier");


			if(obj.getAdresseperso() != null) {
                ps.setLong(8, obj.getAdresseperso().getIdAdresse());
            } else {
                ps.setNull(8, Types.VARCHAR);
            }
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
		


	@Override
	public void update(ClientParticulier obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE clients SET telephone = ?, moyen_paiement = ?, civilite = ?, nom = ?, prenoms = ?, type_instance = ?, ADRESSE_ID_ADRESSE = ? WHERE id = ?");
			
			
			ps.setString(1, obj.getTelephone() );
			ps.setString(2, obj.getMoyenPaiement().toString());
			ps.setString(3, obj.getCivilite().toString());
			ps.setString(4, obj.getNom());
			ps.setString(5, obj.getPrenoms());
			ps.setString(6, "particulier");
			
			if(obj.getAdressefacturation() != null) {
                ps.setLong(7, obj.getAdresseperso().getIdAdresse());
            } else {
                ps.setNull(7, Types.VARCHAR);
            }
			
			ps.setString(8,obj.getMail());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(ClientParticulier obj) {
		deleteById(obj.getMail());
		
	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE clients WHERE id = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
		
}






