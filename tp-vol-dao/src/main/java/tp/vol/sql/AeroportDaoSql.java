package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;


import tp.vol.Application;
import tp.vol.dao.IAeroportDao;
import tp.vol.model.Aeroport;

public class AeroportDaoSql implements IAeroportDao {

	@Override
	public List<Aeroport> findAll() {
		List<Aeroport> aeroports = new ArrayList<Aeroport>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT code FROM aeroports");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				String codeAeroport = rs.getString("code");

				Aeroport aeroport = new Aeroport(codeAeroport);	

				aeroports.add(aeroport);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return aeroports;
	}

	@Override
	public Aeroport findById(String codeAeroport) {
		Aeroport aeroport = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT code FROM aeroports WHERE code = ?");

			ps.setString(1, codeAeroport);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				 codeAeroport = rs.getString("code"); 
				 //TODO verifier code où deja créé
				aeroport = new Aeroport(codeAeroport);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return aeroport;
	}

	@Override
	public void create(Aeroport obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO aeroports (code)VALUES (?)");
			
			ps.setString(1,obj.getCodeAeroport());
			
			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			
	}

	@Override
	public void update(Aeroport obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("UPDATE aeroports SET code = ? WHERE code = ?");

			ps.setString(1, obj.getCodeAeroport());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Aeroport obj) {
		deleteById(obj.getCodeAeroport());
		
	}

	@Override
	public void deleteById(String codeAeroport) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE aeroports WHERE code = ?");

			ps.setString(1, codeAeroport);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
