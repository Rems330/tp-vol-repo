package tp.vol.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.vol.dao.IClientProDao;
import tp.vol.model.Adresse;
import tp.vol.model.Civilite;
import tp.vol.model.ClientPro;
import tp.vol.model.MoyenPaiement;
import tp.vol.model.TypeEntreprise;


public class ClientProDaoSql implements IClientProDao{

	@Override
	public List<ClientPro> findAll() {
		List<ClientPro> clientsPros = new ArrayList<ClientPro>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT mail, telephone, moyen_paiement, numsiret, nom_entreprise, num_tva, type_entreprise, type_instance, ADRESSE_ID_ADRESSE  FROM clients");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String moyenPaiement = rs.getString("moyen_paiement");
				Long numeroSiret = rs.getLong("numsiret");
				String nomEntreprise = rs.getString("nom_entreprise");
				String numTVA = rs.getString("num_tva");
				String typeEntreprise = rs.getString("type_entreprise");
				Long idAdresse = rs.getLong("ADRESSE_ID_ADRESSE");

				
						

				ClientPro clientPro = new ClientPro();
				clientPro.setMail(mail);
				clientPro.setTelephone(telephone);
				clientPro.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
				clientPro.setNumeroSiret(numeroSiret);
				clientPro.setNomEntreprise(nomEntreprise);
				clientPro.setNumTVA(numTVA);
				clientPro.setTypeEntreprise(TypeEntreprise.valueOf(typeEntreprise));
				
				if (idAdresse != null) {
					Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresse);
					clientPro.setAdressefacturation(adresse);
                }

				
				
				
				clientsPros.add(clientPro);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return clientsPros;
	}

	@Override
	public ClientPro findById(String id) {
		ClientPro clientPro = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT  mail, telephone, moyen_paiement, numsiret, nom_entreprise, num_tva, type_entreprise, type_instance, ADRESSE_ID_ADRESSE  FROM clients WHERE id = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String moyenPaiement = rs.getString("moyen_paiement");
				Long numeroSiret = rs.getLong("numsiret");
				String nomEntreprise = rs.getString("nom_entreprise");
				String numTVA = rs.getString("num_tva");
				String typeEntreprise = rs.getString("type_entreprise");
				String typeInstance = rs.getString("type_instance");
				Long idAdresse = rs.getLong("ADRESSE_ID_ADRESSE");
				
						

				clientPro = new ClientPro();
				clientPro.setMail(mail);
				clientPro.setTelephone(telephone);
				clientPro.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
				clientPro.setNumeroSiret(numeroSiret);
				clientPro.setNomEntreprise(nomEntreprise);
				clientPro.setNumTVA(numTVA);
				clientPro.setTypeEntreprise(TypeEntreprise.valueOf(typeEntreprise));
				
				if (idAdresse != null) {
					Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresse);
					clientPro.setAdressefacturation(adresse);
                }
				
				

				
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return clientPro;
	}


	@Override
	public void create(ClientPro obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO clients(mail, telephone, moyen_paiement, numsiret, nom_entreprise, num_tva, type_entreprise, type_instance, ADRESSE_ID_ADRESSE ) VALUES (?,?,?,?,?,?,?,?,?)");
			
			ps.setString(1, obj.getMail());
			ps.setString(2, obj.getTelephone() );
			ps.setString(3, obj.getMoyenPaiement().toString());
			ps.setLong(4, obj.getNumeroSiret());
			ps.setString(5, obj.getNomEntreprise());
			ps.setString(6, obj.getNumTVA());
			ps.setString(7, "particulier");
			
			
			if(obj.getAdressefacturation() != null) {
                ps.setLong(8, obj.getAdressefacturation().getIdAdresse());
            } else {
                ps.setNull(8, Types.VARCHAR);
            }
			
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(ClientPro obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE clients SET telephone = ?, moyen_paiement = ?, numsiret = ?, nom_entreprise = ?, num_tva = ?, type_entreprise = ?, type_instance = ?, ADRESSE_ID_ADRESSE = ? WHERE id = ?");
			
			
			
			ps.setString(1, obj.getTelephone() );
			ps.setString(2, obj.getMoyenPaiement().toString());
			ps.setLong(3, obj.getNumeroSiret());
			ps.setString(4, obj.getNomEntreprise());
			ps.setString(5, obj.getNumTVA());
			ps.setString(6, "particulier");
			
			if(obj.getAdressefacturation() != null) {
				ps.setLong(7, obj.getAdressefacturation().getIdAdresse());
            } else {
                ps.setNull(7, Types.VARCHAR);
            }
			
			
			
			ps.setString(8, obj.getMail());
			
			
			
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(ClientPro obj) {
		deleteById(obj.getMail());
		
	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE clients WHERE id = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
	}

}
