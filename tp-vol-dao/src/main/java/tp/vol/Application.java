package tp.vol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import tp.vol.dao.IAdresseDao;
import tp.vol.dao.IAeroportDao;
import tp.vol.dao.IClientParticulierDao;
import tp.vol.dao.IClientProDao;
import tp.vol.dao.ICompagnieDao;
import tp.vol.dao.IPassagerDao;
import tp.vol.dao.IReservationDao;
import tp.vol.dao.IVilleDao;
import tp.vol.dao.IVolDao;
import tp.vol.dao.IVoyageDao;
import tp.vol.sql.AdresseDaoSql;
import tp.vol.sql.AeroportDaoSql;
import tp.vol.sql.ClientParticulierDaoSql;
import tp.vol.sql.ClientProDaoSql;
import tp.vol.sql.CompagnieDaoSql;
import tp.vol.sql.VilleDaoSql;
import tp.vol.sql.VolDaoSql;
import tp.vol.sql.VoyageDaoSql;

public class Application {
	private static Application instance = null;

	private final IClientParticulierDao clientParticulierDao = new ClientParticulierDaoSql();
	private final IClientProDao clientProDao = new ClientProDaoSql();
	private final IPassagerDao passagerDao = new PassagerDaoSql();
	private final IAdresseDao adresseDao = new AdresseDaoSql();
	private final IReservationDao reservationDao = new ReservationDaoSql();
	private final IVilleDao villeDao = new VilleDaoSql();
	private final IVilleAeroportDao villeAeroportDao = new VilleAeoroportDaoSql();
	private final IVoyageVolDao voyageVolDao = new AeoroportDaoSql();
	private final IVoyageDao voyageDao = new VoyageDaoSql();
	private final IAeroportDao aeroportDao = new AeroportDaoSql();
	private final ICompagnieDao compagnieDao = new CompagnieDaoSql();
	private final IVolDao volDao = new VolDaoSql();

	private Application() {
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Application getInstance() {
		if (instance == null) {
			instance = new Application();
		}

		return instance;
	}

	public IClientParticulierDao getClientParticulierDao() {
		return clientParticulierDao;
	}

	public IClientProDao getClientProDao() {
		return clientProDao;
	}
	public IPassagerDao getPassagerDao() {
		return passagerDao;
	}

	public IAdresseDao getAdresseDao() {
		return adresseDao;
	}

	public ICompagnieDao getCompagnieDao() {
		return compagnieDao;
	}
	
	public IReservationDao getReservationDao() {
		return reservationDao;
	}
	
	public IVilleDao getVilleDao() {
		return villeDao;
	}
	
	public IVilleAeroportDao getVilleAeroportDao() {
		return villeAeroportDao;
	}

	public IVolDao getVolDao() {
		return volDao;
	}
	
	public IVoyageVolDao getVoyageVolDao() {
		return voyageVolDao;
	}
	
	public IVoyageDao getVoyageDao() {
		return voyageDao;
	}
	
	public IAeroportDao getAeoportDao() {
		return aeroportDao;
	}
	

	public IAeroportDao getAeroportDao() {
		return aeroportDao;
	}

	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "vol", "vol");
	}
	


}